const mongoose = require('mongoose');
const { Schema } = mongoose;

const DetailsSchema = new Schema({
    Client_id:{
        type: mongoose.Schema.Types.ObjectId
        // default:mongoose.Schema.Types.ObjectId
    },
    Package_id:{
        type: String,
        required:true
    },
    Start_Date:{
        type:Date,
        default:Date.now
    },
    Company_Name:{
        type: String
    },
    Company_Email:{
        type:String,
        default :"General"
    },
    Contact_No:{
        type:Number
    },
    Website:{
        type:String,
        default :"General"
    },
    Logo:{
        type:String,
        default :"General"
    },
    GSTN:{
        type:String,
        default :"General"
    },
    Tan_No:{
        type:String,
        default :"General"
    },
    Company_address:{
        type:String,
        default :"General"
    }
})

DetailsSchema.pre('save', function (next) {
    this.Client_id = this._id;
    next();
});


module.exports = mongoose.model('details',DetailsSchema);