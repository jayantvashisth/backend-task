const express = require('express');
const router = express.Router();
const Details = require('../models/Details');
const multer  = require('multer')
const upload = multer({ dest: 'uploads/' })

//middleware
// const upload = require("../middleware/upload")

//without using middleware

router.get('/getalldata', async (req, res) => {
    try {
        const details = await Details.find({});     //up and working
        res.json(details)
    } catch (error) {
        console.error(error.message);
        res.status(500).send("Internal Server Error");
    }
})

router.post('/postdetails', upload.single(), async (req, res) => {            //up and working
    try {
        const {
            Client_id, Package_id, Start_Date, Company_Name, Company_Email, Contact_No, Website, Logo, GSTN, Tan_No, Company_address
        } = req.body;
        
        const detail = new Details({
            Client_id, Package_id, Start_Date, Company_Name, Company_Email, Contact_No, Website, GSTN, Tan_No, Company_address
        });
        if(req.file){
            detail.Logo = req.file.path
        }

        const savedDetail = await detail.save();
        res.json(savedDetail)

    } catch (error) {
        console.error(error.message);
        res.status(500).send("Internal Server Error");
    }
})


router.delete('/deletedetails/:id', async (req, res) => {                 //up and working
    try {
        let detail = await Details.findById(req.params.id);
        if (!detail) { return res.status(404).send("Not Found") }

        detail = await Details.findByIdAndDelete(req.params.id)
        res.json({ "Success": "details have been deleted", detail: detail });
    } catch (error) {
        console.error(error.message);
        res.status(500).send("Internal Server Error");
    }
})


router.put('/updatedetails/:id', upload.single(), async (req, res) => {         //up and working
    const {
         Package_id, Start_Date, Company_Name, Company_Email, Contact_No, Website, GSTN, Tan_No, Company_address
    } = req.body;
    try {
        const Logo = req.file.path
        const newDetail = {};
        if (Package_id) { newDetail.Package_id = Package_id };
        if (Company_Name) { newDetail.Company_Name = Company_Name };
        if (Company_Email) { newDetail.Company_Email = Company_Email };
        if (Website) { newDetail.Website = Website };
        if (Contact_No) { newDetail.Contact_No = Contact_No };
        if (Logo) { newDetail.Logo = Logo };
        if (GSTN) { newDetail.GSTN = GSTN };
        if (Tan_No) { newDetail.Tan_No = Tan_No };
        if (Company_address) { newDetail.Company_address = Company_address };
        if (Start_Date) { newDetail.Start_Date = Start_Date };

        let detail = await Details.findById(req.params.id);
        if (!detail) { return res.status(404).send("Not Found") }

        detail = await Details.findByIdAndUpdate(req.params.id, { $set: newDetail }, { new: true })
        res.json({ detail });
    } catch (error) {
        console.error(error.message);
        res.status(500).send("Internal Server Error");
    }
})


















module.exports = router